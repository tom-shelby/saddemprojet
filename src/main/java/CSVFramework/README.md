# Implémentation actuelle

Ce package contient l'implémentation utilisée actuellement, qui est la plus avancée.

Les composants de ce package sont décrits comme tel:

* PACKAGE DesignPatterns
> Ce package contient les différents DesignPatterns que nous avons implémenter
* PACKAGE DesignPatterns.Strategies
> Une STRATEGIE est un moyen d'encapsuler des metadonnées supplémentaires concernant le fichier
> au fil de sa lecture. Ce comportement permet aux Adapters de ne pas dépendre d'un contexte autre
> que la ligne qu'ils traitent actuellement.

* PACKAGE DesignPattern.Adapters
> Un ADAPTER a pour rôle d'adapter une liste de valeurs (issues de la lecture du fichier)
> sous un format spécifique.

* PACKAGE Resolvers
> Ce package contient les implémentations (actuellement une seule) des classes permettant de charger
> les Stratégies (DesignPatterns/Strategies)

---

1. Program
> Point d'entrée du programme.
2. CSVAgent
> Responsable de la logique de lecture du fichier.
> Applique une stratégie de traitement sur chaque ligne lue, dans l'ordre de lecture.
3. CSVStrategist
> Encadre un agent pour lui ordonner l'application d'une stratégie sur un fichier précis
> Il fournit à l'Agent les points d'entrées et de sortie de la stratégie.
4. Client
> Permet la transmission d'informations (nom du fichier d'entrée, quelle opération doit être effectuée)
> entre l'utilisateur et notre application.
5. EnvironmentVars
> Des variables d'environnements.
