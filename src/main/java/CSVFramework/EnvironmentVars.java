package CSVFramework;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class EnvironmentVars {

    public static final Path RESOURCE_PATH      = Paths.get("src", "main", "resources");

    public static final String ENDLINE_SEPARATOR  = System.lineSeparator();
    public static final String VALUE_SEPARATOR  = ";";

    // L'input est traitée puis adaptée à un mode d'affichage dans la console si mit à TRUE
    public static final boolean CONSOLE_DUMP    = false;
    

}
