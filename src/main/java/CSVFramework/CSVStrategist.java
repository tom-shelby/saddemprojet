package CSVFramework;

import CSVFramework.Resolvers.StrategyResolver;
import CSVFramework.DesignPatterns.Strategies.CSVLineProcessingStrategyInterface;

import java.io.*;

/**
 *
 */
public class CSVStrategist {

    protected StrategyResolver strategyResolver;

    public CSVStrategist() {
        this.strategyResolver = new StrategyResolver();
    }

    /**
     * Retourne le StrategyResolver utilisé par le CSVStrategist
     * @return
     */
    public StrategyResolver getStrategyResolver() {
        return strategyResolver;
    }

    /**
     * Récupère les noms des classes inscrites dans le registre du StrategyResolver
     * @return
     */
    public String[] getAvailableStrategiesNames() {
        return this.strategyResolver.getRegistredStrategiesNames();
    }

    /**
     * Récupère les classes inscrites dans le registre du StrategyResolver
     * @see CSVFramework.Resolvers.StrategyResolver
     * @return
     */
    public Class[] getAvailableStrategies() {
        return this.strategyResolver.getClasses();
    }

    /**
     * Essaie d'éxecuter une stratégie et de l'appliquer selon les paramètres de fichiers spécifiés
     * @param strategy
     */
    public CSVStrategist run(Class strategy, File inputFile, File outputFile) {
        try(final InputStream inputStream = new FileInputStream(inputFile)) {
            try(final OutputStream outputStream = EnvironmentVars.CONSOLE_DUMP
                                                  ? System.out
                                                  : new FileOutputStream(outputFile))
            {
                // Notre résolveur vérifie qu'il connaît la classe, et la retourne
                CSVLineProcessingStrategyInterface instance = this.strategyResolver.resolve(strategy);

//                System.out.printf("%-40s : %-30s\n", "Stratégie résolue dans le CSVStrategist",instance.getClass().getSimpleName());
//                System.out.printf("%-40s : %-30s\n", "L'environnement est il en sortie console ?",EnvironmentVars.CONSOLE_DUMP);
//                System.out.printf("%-40s : %-30s\n", "Nom de la sortie actuelle",outputStream.getClass().getSimpleName());

                CSVAgent agent = new CSVAgent(instance);
                agent.process(inputStream, outputStream);

            } catch (IOException e) {
                System.err.println("Impossible d'écrire le fichier de sortie.");
                e.printStackTrace();
            }
        } catch (IOException ioException) {
            System.err.println(EnvironmentVars.RESOURCE_PATH);
            System.err.println("Impossible de lire le fichier d'entrée à l'adresse : " + inputFile.getAbsolutePath());
            ioException.printStackTrace();
        }
        return this;
    }



}
