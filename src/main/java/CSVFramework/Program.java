package CSVFramework;

import CSVFramework.Client.Client;
import CSVFramework.DesignPatterns.Strategies.Concretes.EdgesStrategy;
import CSVFramework.DesignPatterns.Strategies.Concretes.EliminateDoublonsStrategy;
import CSVFramework.DesignPatterns.Strategies.Concretes.ReformatStrategy;
import CSVFramework.DesignPatterns.Strategies.Concretes.STCStrategy;

import java.io.File;
import java.io.IOException;

public class Program implements Runnable {
    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        File inputFile          = new File(EnvironmentVars.RESOURCE_PATH + File.separator + "input_file.csv");
        File reformatFile       = new File(EnvironmentVars.RESOURCE_PATH + File.separator + "reformat.csv");
        File noDuplicatesFile   = new File(EnvironmentVars.RESOURCE_PATH + File.separator + "reformat_no_duplicates.csv");
        File edgesFile          = new File(EnvironmentVars.RESOURCE_PATH + File.separator + "fere.txt");
        File stcFile            = new File(EnvironmentVars.RESOURCE_PATH + File.separator + "stc.txt");

        new CSVStrategist()
            .run(ReformatStrategy.class, inputFile, reformatFile)
            .run(EliminateDoublonsStrategy.class, reformatFile, noDuplicatesFile)
            .run(EdgesStrategy.class, noDuplicatesFile, edgesFile)
            .run(STCStrategy.class, edgesFile, stcFile);
    }

    public static void main(String[] args) throws IOException {
        if(args.length < 1) {
            Client client = new Client(System.in, System.out);
            client.requestAvailableOperations();
        } else {
            long timeBefore = System.currentTimeMillis();
            Program program = new Program();
            program.run();
            long timeAfter = System.currentTimeMillis();
            System.out.println("Execution terminée en: " + (timeAfter-timeBefore) +"ms");
        }
    }
}
