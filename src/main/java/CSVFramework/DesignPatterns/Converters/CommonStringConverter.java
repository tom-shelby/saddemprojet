package CSVFramework.DesignPatterns.Converters;

public class CommonStringConverter implements CSVLineConverterInterface {

    @Override
    public String convert(String[] values, int lineIndex) {

        StringBuilder stringBuilder = new StringBuilder();
        int length = values.length;
        for(int i = 0; i < length; i++) {
            stringBuilder.append(values[i]);

            if (i == (length - 1)) {
                stringBuilder.append('\n');
            } else {
                stringBuilder.append(',')
                             .append(' ');
            }
        }

        return stringBuilder.toString();
    }
}
