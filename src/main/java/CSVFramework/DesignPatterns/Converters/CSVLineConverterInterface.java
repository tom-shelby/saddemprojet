package CSVFramework.DesignPatterns.Converters;

public interface CSVLineConverterInterface {

    final CSVLineConverterInterface CSVLineConverterInterface = null;

    public String convert(String[] values, int lineIndex);
}
