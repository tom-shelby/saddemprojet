package CSVFramework.DesignPatterns.Converters;

import CSVFramework.Client.Utils;

public class ConsoleConverter implements CSVLineConverterInterface {

    @Override
    public String convert(String[] values, int lineIndex) {
        StringBuilder sBuilder = new StringBuilder().append(Utils.ANSI_RESET).append(String.format("%9d.", lineIndex));
        for(String value : values) {
            sBuilder.append("|");
            // Styling
            if(lineIndex == 0 && value.length() > 12) {
                value = value.substring(0, 9);
                value += "...";
            }
            switch(value) {
                case "true":
                    sBuilder.append(Utils.ANSI_GREEN);
                    break;
                case "false":
                    sBuilder.append(Utils.ANSI_RED);
                    break;
                default:
                    sBuilder.append(Utils.ANSI_RESET);
                    break;
            }
            sBuilder.append(String.format("%-12s", value)).append(Utils.ANSI_RESET);
        }
        return sBuilder.append('\n').toString();
    }
}
