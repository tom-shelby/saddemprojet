package CSVFramework.DesignPatterns.Converters;

import CSVFramework.EnvironmentVars;

public class CSVConverter implements CSVLineConverterInterface {
    @Override
    public String convert(String[] values, int lineIndex) {
        StringBuilder stringBuilder = new StringBuilder();

        int length = values.length;
        for(int i = 0; i < length; i++) {
            stringBuilder.append(values[i]);
            stringBuilder.append(EnvironmentVars.VALUE_SEPARATOR);
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
