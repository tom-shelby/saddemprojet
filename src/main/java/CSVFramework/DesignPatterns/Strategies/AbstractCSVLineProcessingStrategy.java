package CSVFramework.DesignPatterns.Strategies;

import CSVFramework.DesignPatterns.Converters.CSVLineConverterInterface;
import CSVFramework.DesignPatterns.Converters.CommonStringConverter;

import java.io.IOException;

/**
 * Une classe héritant de AbstractCSVLineProcessingStrategy est une classe garantissant
 * une uniformité pour le traitement de chaque ligne d'un fichier (CSV ou non) à travers
 * un Adapter
 * @see CSVLineConverterInterface
 */
public abstract class AbstractCSVLineProcessingStrategy implements CSVLineProcessingStrategyInterface{

    protected CSVLineConverterInterface converter;

    public AbstractCSVLineProcessingStrategy() {
        this.converter = new CommonStringConverter();
    }

    /**
     * @param line
     * @param lineIndex
     * @return
     */
    @Override
    public String execute(String line, int lineIndex) throws IOException {
        return null;
    }
}
