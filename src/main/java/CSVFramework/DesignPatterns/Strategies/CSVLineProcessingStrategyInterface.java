package CSVFramework.DesignPatterns.Strategies;

import java.io.IOException;

/**
 * Une CSVLineProcessingStrategy est une interface garantissant
 * une uniformité pour le traitement de chaque ligne d'un fichier (CSV ou non)
 */
public interface CSVLineProcessingStrategyInterface {

    /**
     * Applique une stratégie sur la ligne dont il est question
     * @param line - La ligne dont il est question
     * @param lineIndex - Le n° de la ligne dont il est question
     * @return
     */
    public String execute(String line, int lineIndex) throws IOException;
}
