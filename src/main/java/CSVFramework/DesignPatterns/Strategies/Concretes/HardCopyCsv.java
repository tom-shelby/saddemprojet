package CSVFramework.DesignPatterns.Strategies.Concretes;

import CSVFramework.DesignPatterns.Converters.CommonStringConverter;
import CSVFramework.EnvironmentVars;
import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;

import java.io.*;

public class HardCopyCsv extends AbstractCSVLineProcessingStrategy {

    public HardCopyCsv() {
        this.converter = new CommonStringConverter();
    }

    @Override
    public String execute(String line, int lineIndex) throws IOException {
        return this.converter.convert(line.split(EnvironmentVars.VALUE_SEPARATOR), lineIndex);
    }
}
