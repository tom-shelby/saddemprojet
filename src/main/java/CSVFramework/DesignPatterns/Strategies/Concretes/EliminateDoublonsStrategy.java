package CSVFramework.DesignPatterns.Strategies.Concretes;

import CSVFramework.DesignPatterns.Converters.CSVConverter;
import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;
import CSVFramework.EnvironmentVars;

import java.io.IOException;
import java.util.Arrays;

public class EliminateDoublonsStrategy extends AbstractCSVLineProcessingStrategy {

    private String[] previousComponents;

    public EliminateDoublonsStrategy() {
        this.converter = new CSVConverter();
    }

    @Override
    public String execute(String line, int lineIndex) throws IOException {
        final String[] currentComponentsWithDatetime = line.split(EnvironmentVars.VALUE_SEPARATOR);

        final String[] currentComponents = Arrays.copyOf(currentComponentsWithDatetime, currentComponentsWithDatetime.length-1);

        String output = this.converter.convert(currentComponentsWithDatetime, lineIndex);
        if(lineIndex > 1) {
            int champsIdentiques = 0;

            final int currentComponentsLength = currentComponents.length;

            for(int i = 0; i < currentComponentsLength && champsIdentiques == i; i++) {
                if(currentComponents[i].equals(this.previousComponents[i]) ) {
                    champsIdentiques++;
                }
            }

            if(champsIdentiques == currentComponents.length) {
                // On supprime la ligne
                output = null;
            }
        }
        this.previousComponents = currentComponents;
        return output;
    }
}
