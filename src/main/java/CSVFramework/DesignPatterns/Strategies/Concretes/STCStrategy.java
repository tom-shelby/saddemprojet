package CSVFramework.DesignPatterns.Strategies.Concretes;

import CSVFramework.DesignPatterns.Converters.STC2GraphConverter;
import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;

import java.io.IOException;
import java.util.Arrays;

public class STCStrategy extends AbstractCSVLineProcessingStrategy {

    /**
     * Components précédents (sans delta)
     */
    protected String[] previousLineComponents;

    public STCStrategy() {
        this.converter = new STC2GraphConverter();
    }

    public String execute(String currentLine, int lineIndex) throws IOException {

        // Ligne du fichier fere
        final String[] currentComponents = currentLine.split(", ");
        final String[] currentComponentsNoDelta = Arrays.copyOf(currentComponents, currentComponents.length - 1);

        final String currentDelta = currentComponents[currentComponents.length-1];

        final StringBuilder out = new StringBuilder();
        switch (lineIndex) {
            case 0:
                final String firstLine = this.formatFirstLine(currentComponentsNoDelta);
                out.append(firstLine);
                break;
            default:
                final String line = this.formatLine(currentComponentsNoDelta, currentDelta);
                out.append(line);
                break;
        }
        this.previousLineComponents = currentComponentsNoDelta;

        return out.toString();
    }

    private String formatFirstLine(String[] values) {
        final StringBuilder output = new StringBuilder(
                String.format("(IN, %s, nct)", values[0])
        );
        output.append(" ").append("*").append(" ");
        for(int i = 1; i < values.length; i++) {
            output.append(String.format("(IN, %s, nct)", values[i]))
                  .append(" ").append("*").append(" ");
        }
        output.append("\n");
        return output.toString();
    }

    private String formatLine(String[] values, String delta) {
        final StringBuilder output = new StringBuilder();

        for(int i = 0; i < previousLineComponents.length; i++) {
            for(int j = 0; j < values.length; j++) {
                output.append(
                        String.format("(%s, %s, %s)", previousLineComponents[i], values[j], delta)
                )
                .append(" ").append("*").append(" ");
            }
            output.append("\n");
        }
        return output.toString();
    }
}
