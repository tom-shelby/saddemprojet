package CSVFramework.DesignPatterns.Strategies.Concretes;

import CSVFramework.DesignPatterns.Converters.CSVConverter;
import CSVFramework.DesignPatterns.Converters.ConsoleConverter;
import CSVFramework.EnvironmentVars;
import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReformatStrategy extends AbstractCSVLineProcessingStrategy {

    private final String[] ignored;
    private final String[] formatted;

    private String[] preHeaders;
    private String[] prodHeaders;

    /**
     * Constructor
     * @param ignored
     * @param formatted
     */
    public ReformatStrategy(String[] ignored, String[] formatted) {
        this.converter = EnvironmentVars.CONSOLE_DUMP
                ? new ConsoleConverter()
                : new CSVConverter();
        this.ignored = ignored;
        this.formatted = formatted;
    }

    private void setPreHeaders(String[] headers) {
        this.preHeaders = headers;
    }

    private void setPostHeaders(String[] headers) {
        this.prodHeaders = headers;
    }

    @Override
    public String execute(String line, int lineIndex) throws IOException {
        final String[] values = line.split(EnvironmentVars.VALUE_SEPARATOR);
        String[] reformattedValues = null;
        switch (lineIndex) {
            case 0:
                reformattedValues = this.processHeaders(values, lineIndex);
                break;
            default:
                reformattedValues = this.processLine(values, lineIndex);
                break;
        }

        //Adapter Hook
        final String adaptedLine = this.converter.convert(reformattedValues, lineIndex);

        return adaptedLine;
    }

    private String[] processHeaders(String[] line, int lineIndex) {
        this.setPreHeaders(line);

        final String[] producedHeaders = this.parseValues(line, lineIndex);
        this.setPostHeaders(line);

        return producedHeaders;
    }

    private String[] processLine(String[] line, int lineIndex) {
        String[] values = this.parseValues(line, lineIndex);
        return values;
    }

    private String[] parseValues(String[] values, int lineIndex) {
        List<String> effectiveValues = new ArrayList<String>();

        for(int i = 0; i < values.length; ++i) {
            String value = values[i];
            if(!this.isAnIgnoredField(i, value)) {
                if(this.isADirtyField(i, value, lineIndex)) {
                    final String date = value.substring(value.indexOf("(")+1, value.indexOf(")"));
                    // {"2020", "8", "24", "12", "58", "8", "943587"}
                    final String[] dateExploded = date.split(", ");
                    value = this.formatDate(dateExploded);
                }
                effectiveValues.add(value);
            }
        }
        return effectiveValues.toArray(new String[0]);
    }

    private boolean isADirtyField(int index, String value, int lineIndex) {
        final List<String> listFormatted = (List<String>) Arrays.asList(this.formatted);
        return lineIndex != 0 && listFormatted.contains(this.preHeaders[index]);
    }

    private boolean isAnIgnoredField(int index, String value) {
        final List<String> listIgnored = Arrays.asList(this.ignored);
        return listIgnored.contains(value) || listIgnored.contains(this.preHeaders[index]);
    }

    /**
     * Reconstruit une date au format humain depuis un tableau comme tel:
     * @param args - {"2020", "8", "24", "12", "58", "8", "943587"}
     * @return String
     */
    private String formatDate(String[] args) {
        final String date = String.format("%04d-%02d-%02d %02d:%02d:%02d.%s",
                Integer.parseInt(args[0]),
                Integer.parseInt(args[1]),
                Integer.parseInt(args[2]),
                Integer.parseInt(args[3]),
                Integer.parseInt(args[4]),
                Integer.parseInt(args[5]),
                String.format("%6s", args[6]).replace(' ', '0')
        );
        return date;
    }
}
