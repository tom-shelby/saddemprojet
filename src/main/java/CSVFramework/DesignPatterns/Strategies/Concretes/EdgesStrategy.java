package CSVFramework.DesignPatterns.Strategies.Concretes;

import CSVFramework.DesignPatterns.Converters.CommonStringConverter;
import CSVFramework.EnvironmentVars;
import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EdgesStrategy extends AbstractCSVLineProcessingStrategy {

    protected String[] headers;
    protected String[] previousLineComponents;

    public EdgesStrategy() {
        this.converter = new CommonStringConverter();
    }

    /**
     * @param currentLine
     * @param lineIndex
     * @return
     */
    @Override
    public String execute(String currentLine, int lineIndex) throws IOException {

        String[] currentLineComponents = currentLine.split(EnvironmentVars.VALUE_SEPARATOR);
        String newLine = "";
        switch(lineIndex) {
            case 0:
                this.computeHeaders(currentLineComponents);
                break;
            case 1:
                // La ligne 1 n'a pas de FE/RE => on l'ignore
                break;
            default:
                String[] currentAnalysis = this.computeEdges(currentLineComponents);
                newLine = this.converter.convert(currentAnalysis, lineIndex);
                break;
        }
        this.previousLineComponents = currentLineComponents;

        return newLine;
    }

    public void computeHeaders(String[] currentLineComponents) {
        this.headers = currentLineComponents;
    }

    public String[] computeEdges(String[] currentLineComponents) {

        ArrayList<String> status = new ArrayList<String>();
        int length = currentLineComponents.length;

        // Pour chaque colonnes des 2 lignes :
        for(int i = 0; i < length; i++) {
            String prevComp = this.previousLineComponents[i];
            String curComp = currentLineComponents[i];

            boolean pComp = Boolean.parseBoolean(prevComp);
            boolean cComp = Boolean.parseBoolean(curComp);

            // Prev: false : Curr: true
            if(!pComp && cComp) {
                status.add("RE_"+headers[i]);
            }
            // Prev: true : Curr: false
            if(pComp && !cComp) {
                status.add("FE_"+headers[i]);
            }
        }

        // Calcul de l'intervalle de temps
        if(status.size() != 0) {
            final int delta = this.computeDelta(previousLineComponents[14], currentLineComponents[14]);
            status.add(delta+"µs");
        }

        return status.toArray(new String[0]);
    }

    private int computeDelta(String previousLineTimestampedField, String currentLineTimestampedField) {
        final String regex = "\\d+:\\d+:\\d+.\\d+$";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher pMatcher = pattern.matcher(previousLineTimestampedField);
        final Matcher cMatcher = pattern.matcher(currentLineTimestampedField);

        // Execute les regex
        pMatcher.find();
        cMatcher.find();
        final String pTimeStr = pMatcher.group(0);
        final String cTimeStr = cMatcher.group(0);

        // Conversions str => heures/minutes/secondes
        final String[] pTimeStrArr = pTimeStr.split(":");
        final String[] cTimeStrArr = cTimeStr.split(":");
        ////* Conversion pour le previous
        final int pTimeH    = Integer.parseInt(pTimeStrArr[0]);
        final int pTimeM    = Integer.parseInt(pTimeStrArr[1]);
        final double pTimeS = Double.parseDouble(pTimeStrArr[2]);
        ////* Conversion pour le current
        final int cTimeH    = Integer.parseInt(cTimeStrArr[0]);
        final int cTimeM    = Integer.parseInt(cTimeStrArr[1]);
        final double cTimeS = Double.parseDouble(cTimeStrArr[2]);

        //Calcul du cumul de secondes (à partir de 00:00)
        final double pSeconds = (pTimeH*3600) + (pTimeM*60) + pTimeS;
        final double cSeconds = (cTimeH*3600) + (cTimeM*60) + cTimeS;
        //Soustraction des deux cumuls * 1000000 = différence en ms
        final double doubleDelta = (cSeconds - pSeconds) * 1000000;
        final int delta = (int) doubleDelta;

        // System.out.println(delta + "µs");
        return delta;
    }
}
