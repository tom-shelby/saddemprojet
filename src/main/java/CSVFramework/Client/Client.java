package CSVFramework.Client;

import CSVFramework.CSVStrategist;
import CSVFramework.EnvironmentVars;

import java.io.*;
import java.nio.file.Path;
import java.util.Scanner;

public class Client {

    /**
     * Flux d'entrée du Client
     * @field InputStream
     */
    private final InputStream inputStream;
    /**
     * Utilitaire de lecture sur le flux d'entrée du Client
     * @field Scanner
     */
    private final Scanner scanner;
    /**
     * Flux de sortie du Client
     * @field OutputStream
     */
    private final OutputStream outputStream;
    /**
     * Utilitaire d'écriture sur le flux de sortie du Client
     * @field OutputStreamWriter
     */
    private final OutputStreamWriter outputStreamWriter;
    /**
     * Le stratège propre au Client
     * @field CSVStrategist
     */
    private final CSVStrategist CSVStrategist;

    /**
     * Constructeur
     * @param inputStream
     * @param outputStream
     */
    public Client(InputStream inputStream, OutputStream outputStream) {
        this.inputStream    = inputStream;
        this.outputStream   = outputStream;

        this.scanner = new Scanner(this.inputStream);
        this.outputStreamWriter = new OutputStreamWriter(this.outputStream);

        this.CSVStrategist = new CSVStrategist();
    }

    /**
     * Construit un message pour la console
     * @param rawMessage
     * @return String
     */
    private String buildMessage(String... rawMessage) {
        StringBuilder message = new StringBuilder();

        message.append("////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
               .append('\n');

        int outputMaxSize = 122;

        for(String msg : rawMessage) {
            final int length = msg.length();
            for (int i = 0; i < length; i += outputMaxSize) {
                StringBuilder _msg = new StringBuilder();
                if (i + outputMaxSize > length) {
                    _msg.append(msg.substring(i));
                } else {
                    _msg.append(msg.substring(i, outputMaxSize));
                }
                message.append(String.format("|| %-122s ||", _msg))
                       .append('\n');
            }
        }
        message.append("////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
               .append('\n');
        return message.toString();
    }

    /**
     * Affiche un message à l'utilisateur
     * @param rawMessage
     * @throws IOException
     */
    private void showMessage(String... rawMessage) throws IOException {
        String message = this.buildMessage(rawMessage);

        this.outputStreamWriter.write(message);
        this.outputStreamWriter.flush();
    }

    private String[] getOptions() {
        final String[] avalaibles = this.CSVStrategist.getAvailableStrategiesNames();
        final int length = avalaibles.length;
        String[] options = new String[length];
        for(int i = 0; i < length; i++) {
            options[i] = String.format("%d - %s", i+1, avalaibles[i]);
        }

        return options;
    }

    private void showInputMessage() throws IOException {
        this.outputStreamWriter.write("> ");
        this.outputStreamWriter.flush();
    }

    public void requestAvailableOperations() throws IOException {
        this.showMessage("Choisissez une opération a effectuer :");
        this.showMessage(this.getOptions());
        this.showInputMessage();

        int selected = this.getUserSelection() - 1;
        System.out.print(Utils.CLEAR);
        System.out.flush();

        Path filePath = EnvironmentVars.RESOURCE_PATH;

        this.showMessage("Les fichiers sont situés dans le répertoire :", filePath.toAbsolutePath().toString());
        this.showMessage("Saisissez une valeur pour le fichier d'entrée");
        String inputFilename    = this.requestStringValue();
        File inputFile          = new File(filePath + File.separator + inputFilename);

        this.showMessage("Saisissez une valeur pour le fichier de sortie");
        String outputFilename   = this.requestStringValue();
        File outputFile         = new File(filePath + File.separator + outputFilename);

        this.CSVStrategist.run(this.CSVStrategist.getAvailableStrategies()[selected], inputFile, outputFile);
    }

    private void showErrorMessage() throws IOException {
        this.outputStreamWriter.write(Utils.ANSI_RED+"Veuillez réessayez\n"+Utils.ANSI_RESET);
        this.outputStreamWriter.flush();
    }

    private int getUserSelection() throws IOException {
        int selection = -1;
        boolean unvalidated = true;

        while(unvalidated) {
            String userInput = this.scanner.nextLine();
            try {
                selection = Integer.parseInt(userInput);
                if(selection > 0 && selection <= this.getOptions().length) {
                    unvalidated = false;
                }
            } catch(NumberFormatException numberFormatException) {
                this.showErrorMessage();
                this.showInputMessage();
                continue;
            }
        }
        return selection;
    }

    private String requestStringValue() throws IOException {
        this.showInputMessage();
        return scanner.nextLine();
    }

    private void execute() throws IOException {
        while(true) {
            this.requestAvailableOperations();
        }

    }
}
