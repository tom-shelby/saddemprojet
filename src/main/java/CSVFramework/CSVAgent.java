package CSVFramework;

import CSVFramework.DesignPatterns.Strategies.CSVLineProcessingStrategyInterface;

import java.io.*;

/**
 * La classe CSVAgent encapsule une stratégie.
 * L'Agent est géré par le Strategist.
 *
 * L'agent a pour simple rôle de lire le fichier, et d'éxecuter la stratégie adéquate.
 */
public class CSVAgent {

    protected CSVLineProcessingStrategyInterface strategy;

    public CSVAgent(CSVLineProcessingStrategyInterface strategy) {
        this.strategy = strategy;
    }

    public int process(InputStream inputStream, OutputStream outputStream) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        int lineIndex = 0;
        String line = reader.readLine();
        do{
            // On applique la stratégie à la ligne qui vient d'être récupérée par le reader.
            // La stratégie se voit décharger le comportement de transformation vers un nouveau format au travers d'un Adapter.
            final String result = this.strategy.execute(line, lineIndex);

            if(result != null) {
                outputStream.write(result.getBytes());
            }


            // On modifie nos itérateurs
            line = reader.readLine();
            lineIndex++;
        } while (line != null);

        outputStream.flush();
        return 1;
    }
}
