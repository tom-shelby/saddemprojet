package CSVFramework.Resolvers;

import CSVFramework.DesignPatterns.Strategies.AbstractCSVLineProcessingStrategy;
import CSVFramework.DesignPatterns.Strategies.CSVLineProcessingStrategyInterface;
import CSVFramework.DesignPatterns.Strategies.Concretes.*;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class StrategyResolver {

    protected HashMap<String, Class> interfaceHashMap;

    public StrategyResolver() {
        this.interfaceHashMap = new HashMap<String, Class>();

        this.initRegistry();
    }

    public String[] getRegistredStrategiesNames() {
        String[] values = new String[this.interfaceHashMap.size()];
        return this.interfaceHashMap.keySet().toArray(values);
    }

    public Class[] getClasses() {
        Class[] values = new Class[this.interfaceHashMap.size()];
        return this.interfaceHashMap.values().toArray(values);
    }

    public CSVLineProcessingStrategyInterface resolve(Class classe)
    {
        System.out.println(classe.getSimpleName());
        CSVLineProcessingStrategyInterface instance = null;
        final String className = classe.getSimpleName();
        try {
            if(this.interfaceHashMap.containsKey(className)) {
                // Cas spécial pour reformatter selon des champs prédéfinis
                if(className.equals(ReformatStrategy.class.getSimpleName())) {
                    final String[] ignoredHeaders = {"Counter", "SP_X", "SP_Y", "SP_Z", "X", "Y", "Z"};
                    final String[] formatted = {"Temps_occurrence"};
                    final Class[] args = {ignoredHeaders.getClass(), formatted.getClass()};

                    Constructor constructor = classe.getDeclaredConstructor(args);
                    instance = (CSVLineProcessingStrategyInterface) constructor.newInstance(ignoredHeaders, formatted);
                } else {
                    Constructor constructor = classe.getDeclaredConstructor();
                    instance = (CSVLineProcessingStrategyInterface) constructor.newInstance();
                }
            } else {
                final String msg = String.format("%s, %s", "Classe non reconnue", className);
                throw new RuntimeException(msg);
            }
        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return instance;
    }

    /**
     * Ajoute au registre des opérations exécutables par l'utilisateur
     */
    public void initRegistry() {
        this.registerAll(
            ReformatStrategy.class,
            EliminateDoublonsStrategy.class,
            EdgesStrategy.class,
            STCStrategy.class,
            HardCopyCsv.class
        );
    }

    public void register(Class strategyInterface) {
        final String key = strategyInterface.getSimpleName();

        if(!this.interfaceHashMap.containsKey(key)) {
            this.interfaceHashMap.put(key, strategyInterface);
        }
    }

    public void registerAll(Class... possibleStrategiesClasses) {
        for(Class possibleStrategy : possibleStrategiesClasses) {
            if (possibleStrategy.getSuperclass().equals(AbstractCSVLineProcessingStrategy.class)) {
                this.register(possibleStrategy);
            }
        }
    }
}
