package old;

import java.io.*;

/**
 *
 * @author Baptiste CATOIS
 * @author Clément WALTHER
 * @author Esteban MOLL
 * @author Maxance COTHENET
 * =====================================================================================================================
 * Pour l'instant il faut relancer le programme à chaque étape (mais tout fonctionne correctement)
 * => Exception in thread "main" java.lang.NullPointerException
 * Le fichier de l'étape précédente n'existe pas encore et comme on instancie un old.CSVManipulator sur le fichier de
 * l'étape précédente, ça peut donc planter pour des configs dépourvues SSD.
 *
 * Sinon, je le répète, mais ça fonctionne.
 */
public class Main {

    /**
     * Notre programme
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // Créer un objet permettant de manipuler nos CSV
        CSVManipulator manipulator = new CSVManipulator("input_file.csv", ';');
        // Créer un fichier qui sera utilisé en sortie
        File reformatFile = new File(CSVManipulator.RESSOURCE_DIR+ File.separator + "reformat.csv");
        // Créer un flux de sortie vers le fichier crée plus haut
        FileOutputStream fileOutput = new FileOutputStream(reformatFile);
        // Le manipulateur reformatte comme demandé dans la consigne de Mme Ramla SADDEM.
        // Le résultat est envoyé dans l'interface de flux spécifié en paramètre.
        manipulator.reformat(fileOutput);

        // Idem qu'au dessus mais pour retirer les doublons
        File noDoublonFile = new File(CSVManipulator.RESSOURCE_DIR+ File.separator + "no_doublon.csv");
        FileOutputStream doublonOutput = new FileOutputStream(noDoublonFile);
        CSVManipulator manipulator2 = new CSVManipulator("reformat.csv", ';');
        manipulator2.noDoublon(doublonOutput);
        
        //Idem seulement pour les FE/RE
        File edgesFile = new File(CSVManipulator.RESSOURCE_DIR+ File.separator + "edges.txt");
        FileOutputStream edgesOutput = new FileOutputStream(edgesFile);
        CSVManipulator manipulator3 = new CSVManipulator("no_doublon.csv", ';');
        manipulator3.edges(edgesOutput);
    }
}
