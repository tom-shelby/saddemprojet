package old;

import CSVFramework.EnvironmentVars;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CSVManipulator {

    final static Path RESSOURCE_DIR = Paths.get("src", "main", "resources");

    protected final BufferedReader reader;
    protected final char separator;
    /**
     * Permet de créer un objet manipulant un fichier CSV dont le nom est spécifiée en paramètre. Ce fichier doit être présent dans src/main/resources/
     * @param inputFileName
     * @throws IOException
     */
    public CSVManipulator(String inputFileName, char separator) throws IOException {
        final InputStream resource = new FileInputStream(EnvironmentVars.RESOURCE_PATH + File.separator + inputFileName);

        this.separator = separator;
        this.reader = new BufferedReader(new InputStreamReader(resource));
    }

    /**
     * Depuis le fichier formatté
     * @param outputStream
     * @throws IOException
     */
    public void noDoublon(OutputStream outputStream) throws IOException {
        boolean isHeaderLine    = true;                     // Default to true
        String line             = this.reader.readLine();   // Default to first line (headers)
        String prevLine         = null;                     // Default to null
        String headers          = null;                     // Default to null

        while (line != null) {
            if(!isHeaderLine && !prevLine.equals(headers)) {

                final String[] prevSplited      = prevLine.split(String.valueOf(this.separator));
                final String[] lineSplited      = line.split(String.valueOf(this.separator));

                int identical = 0;
                for(int i = 0; i < lineSplited.length-1; i++) {
                    final boolean prevValue = Boolean.parseBoolean(prevSplited[i]);
                    final boolean curValue  = Boolean.parseBoolean(lineSplited[i]);
                    if(prevValue == curValue) {
                        identical++;
                    }
                }
                if(identical != lineSplited.length-1) {
                    outputStream.write(line.getBytes());
                    // Nouvelle ligne et écriture du flux
                    outputStream.write('\n');
                    outputStream.flush();
                }
            }

            //Reset for manipulating on non heading lines
            if(isHeaderLine) {
                outputStream.write(line.getBytes());
                outputStream.write('\n');
                outputStream.flush();
                headers = line;
                isHeaderLine = false;
            }
            // next iteration on next line
            prevLine = line;
            line = this.reader.readLine();
        }
    }

    /**
     * A partir du fichier `input_file.csv` UNIQUEMENT :
     * - Elimine les colonnes concernant les positions des capteurs (valeurs flottantes)
     * - Formatte la Date
     * @param outputStream
     * @throws IOException
     */
    public void reformat(OutputStream outputStream) throws IOException {
        String line = this.reader.readLine();
        boolean isHeaderLine = true;

        while (line != null) {
            final String[] splited = line.split(String.valueOf(this.separator));

            for(int i = 0; i < splited.length; i++) {
                if(i < 13 || i > 20 ) {
                    if (i == splited.length-1 && !isHeaderLine) { // Colonne `Temps_Occurence`
                        final String date = splited[i].substring(splited[i].indexOf("(")+1, splited[i].indexOf(")"));
                        final String[] dateExploded = date.split(", ");
                        final String dateStr = this.formatDate(dateExploded);
                        outputStream.write(dateStr.getBytes());
                    }
                    else { // Toute autre colonne
                        outputStream.write(splited[i].getBytes());
                    }
                    if(i < splited.length-1) { // Ajout du séparateur partout sauf en fin de chaîne
                        outputStream.write(this.separator);
                    }
                }
            }
            // Nouvelle ligne et écriture du flux
            outputStream.write('\n');
            outputStream.flush();

            //Reset for manipulating on non heading lines
            if(isHeaderLine) {
                isHeaderLine = false;
            }
            // next iteration on next line
            line = this.reader.readLine();
        }
    }

    /**
     * Reconstruit une date au format humain depuis un tableau comme tel:
     * @param args - {"2020", "8", "24", "12", "58", "8", "943587"}
     * @return String
     */
    private String formatDate(String[] args) {
        final String date = String.format("%04d-%02d-%02d %02d:%02d:%02d.%d",
                Integer.parseInt(args[0]),
                Integer.parseInt(args[1]),
                Integer.parseInt(args[2]),
                Integer.parseInt(args[3]),
                Integer.parseInt(args[4]),
                Integer.parseInt(args[5]),
                Integer.parseInt(args[6])
        );
        return date;
    }

    /**
     * Lit un fichier dans la sortie spécifiée
     * @param outputStream
     * @throws IOException
     */
    public void completeRead(OutputStream outputStream) throws IOException {
        String line = this.reader.readLine();
        while (line != null) {
            outputStream.write(line.getBytes());
            outputStream.write('\n');

            // next iteration on next line
            line = this.reader.readLine();
        }
        outputStream.flush();
    }


    /**
     * Compare deux lignes et retourne un tableau avec les FE et RE des différents capteurs
     * @param prevLine
     * @param curLine
     * @param headers
     * @return
     * @throws IllegalArgumentException
     */
    public String[] evaluateEdges(String prevLine, String curLine, String[] headers) throws IllegalArgumentException
    {
        // On éclate les chaînes en plusieurs composants séparés par un ';'
        String[] pLineArr = prevLine.split(";");
        String[] cLineArr = curLine.split(";");

        // Notre tableau de retour
        ArrayList<String> status = new ArrayList<String>();

        if(pLineArr.length != cLineArr.length) {
            System.out.println("Ca ne devrait pas avoir lieu");
            throw new IllegalArgumentException("Les deux lignes n'ont pas le même nombres de colonnes");
        }

        // Pour chaque colonnes des 2 lignes :
        for(int i = 0; i < pLineArr.length; i++) {
            String prevComp = pLineArr[i];
            String curComp = cLineArr[i];

            boolean pComp = Boolean.parseBoolean(prevComp);
            boolean cComp = Boolean.parseBoolean(curComp);

            // Prev: false : Curr: true
            if(!pComp && cComp) {
                status.add("RE_"+headers[i]);
            }
            // Prev: true : Curr: false
            if(pComp && !cComp) {
                status.add("FE_"+headers[i]);
            }
        }

        // Calcul de l'intervalle de temps
        if(status.size() != 0) {
            final String regex = "\\d+:\\d+:\\d+.\\d+$";
            final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            final Matcher pMatcher = pattern.matcher(pLineArr[13]);
            final Matcher cMatcher = pattern.matcher(cLineArr[13]);

            // Execute les regex
            pMatcher.find();
            cMatcher.find();
            final String pTimeStr = pMatcher.group(0);
            final String cTimeStr = cMatcher.group(0);

            // Conversions str => heures/minutes/secondes
            final String[] pTimeStrArr = pTimeStr.split(":");
            final String[] cTimeStrArr = cTimeStr.split(":");
            ////* Conversion pour le previous
            final int pTimeH    = Integer.parseInt(pTimeStrArr[0]);
            final int pTimeM    = Integer.parseInt(pTimeStrArr[1]);
            final double pTimeS = Double.parseDouble(pTimeStrArr[2]);
            ////* Conversion pour le current
            final int cTimeH    = Integer.parseInt(cTimeStrArr[0]);
            final int cTimeM    = Integer.parseInt(cTimeStrArr[1]);
            final double cTimeS = Double.parseDouble(cTimeStrArr[2]);

            //Calcul du cumul de secondes (à partir de 00:00)
            final double pSeconds = (pTimeH*3600) + (pTimeM*60) + pTimeS;
            final double cSeconds = (cTimeH*3600) + (cTimeM*60) + cTimeS;
            //Soustraction des deux cumuls * 1000000 = différence en ms
            final double doubleDelta = (cSeconds - pSeconds) * 1000000;
            final int delta = (int) doubleDelta;

            // System.out.println(delta + "µs");
            status.add(delta + "µs");
        }

        return status.toArray(new String[0]);
    }

    /**
     * Analyse les FE/RE pour chaque ligne que manipule le old.CSVManipulator
     * @param outputStream
     * @throws IOException
     */
    public void edges(OutputStream outputStream) throws IOException {
        String line = this.reader.readLine();
        boolean isHeaderLine = true;
        boolean isFirstLine = true;
        String prevLine = null;
        String[] headers = null;

        while(line != null ) {
            if(isHeaderLine) {
                headers = line.split(String.valueOf(this.separator));

                // Reset
                isHeaderLine = false;
                line = this.reader.readLine();
                continue;
            }

            // Après passage des headers, on est sur la première ligne
            if(isFirstLine) {
                // On évalue pas pcq pas d'ancienne ligne
                isFirstLine = false;
                prevLine = line;
                line = this.reader.readLine();
                continue;
            }

            // On évalue les Falling Edge (FE) et Rising Edge (RE)
            String[] edges = this.evaluateEdges(prevLine, line, headers);
            // System.out.println(Arrays.toString(edges));

            // Ecriture des fronts
            for(String data : edges) {
                outputStream.write(data.getBytes());
                outputStream.write(',');
            }
            outputStream.write('\n');
            outputStream.flush();

            // Reset pour prochaine étape de la boucle
            prevLine = line;
            line = this.reader.readLine();
        }
    }

    public static void main(String[] args) throws IOException {
        // The cake is a lie
        OutputStream stream = new FileOutputStream(new File(EnvironmentVars.RESOURCE_PATH + File.separator + "output.csv"));
        CSVManipulator manipulator = new CSVManipulator("input_file.csv", ';');

        manipulator.noDoublon(stream);
    }


}
